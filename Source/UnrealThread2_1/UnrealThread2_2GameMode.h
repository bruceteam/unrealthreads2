// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealThread2_2GameMode.generated.h"

/**
 * 
 */


class AWallActor;

UCLASS()
class UNREALTHREAD2_1_API AUnrealThread2_2GameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MazeSizeX = 20;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MazeSizeY = 20;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AWallActor> WallActor = nullptr;

	UFUNCTION(BlueprintCallable)
	void GenerateMaze();

	UFUNCTION(BlueprintCallable)
	void SpawnWalls();

	UPROPERTY(BlueprintReadWrite)
	bool bIsGenerated = false;


protected:
	TArray<bool*> CellContainer;
	TArray<TArray<bool>> Maze;

	void PrintMazeInLog();
	bool CheckClosedCells(int X, int Y);
	void Exits();
};
