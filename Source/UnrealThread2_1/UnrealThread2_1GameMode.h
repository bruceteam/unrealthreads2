// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealThread2_1GameMode.generated.h"


UCLASS()
class UNREALTHREAD2_1_API AUnrealThread2_1GameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	void DoThreads();

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		int SizeOfArray = 10;

	TArray<int> ArrayOfIntegers;

	void PrintArray(bool bIsSorted);
};
