// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealThread2_2GameMode.h"
#include "WallActor.h"

void AUnrealThread2_2GameMode::GenerateMaze()
{
	CellContainer.Empty();
	Maze.Empty();
	bIsGenerated = false;

	/*������� ������ ��������� � ��������� "�����������" �� �������
	* * * * *
	* * * * *
	* * * * *
	* * * * *
	* * * * *
	*/
	Maze.SetNum(MazeSizeX);

	for (int i = 0; i < MazeSizeX; i++)
	{
		Maze[i].SetNum(MazeSizeY);
		for (int j = 0; j < MazeSizeY; j++)
		{
			Maze[i][j] = true;
		}
	}

	/*������� ������� � ������
	* * * * *
	0 * * * *
	* * * * 0
	* * * * *
	* * * * *
	*/
	Exits();

	int count = 0;

	while (count <= MazeSizeX * MazeSizeY / 4)
	{
		FPlatformProcess::Sleep(0.0003);
		ParallelFor(2, [&](int32)
			{
				int RandX = FMath::RandRange(1, MazeSizeX - 2);
				int RandY = FMath::RandRange(1, MazeSizeY - 2);
				int randDir = FMath::RandRange(0, 3);

				switch (randDir)
				{
				case 0: //right
				{
					int NewY = RandY + 2;
					if (NewY >= MazeSizeY - 2)
						break;
					else
					{
						if (CellContainer.Contains(&Maze[RandX][RandY]) && CellContainer.Contains(&Maze[RandX][NewY]))
							break;
						else if (CheckClosedCells(RandX, RandY))
							break;
						else
						{
							CellContainer.Add(&Maze[RandX][RandY]);
							CellContainer.Add(&Maze[RandX][NewY]);
							count++;
							Maze[RandX][RandY] = false;
							Maze[RandX][NewY] = false;
							Maze[RandX][NewY - 1] = false;
							break;
						}
					}
				}
				case 1:	//down
				{
					int NewX = RandX + 2;
					if (NewX >= MazeSizeX - 2)
						break;
					else
					{
						if (CellContainer.Contains(&Maze[RandX][RandY]) && CellContainer.Contains(&Maze[NewX][RandY]))
							break;
						else if (CheckClosedCells(RandX, RandY))
							break;
						else
						{
							CellContainer.Add(&Maze[RandX][RandY]);
							CellContainer.Add(&Maze[NewX][RandY]);
							count++;
							Maze[RandX][RandY] = false;
							Maze[NewX][RandY] = false;
							Maze[NewX - 1][RandY] = false;
							break;
						}
					}
				}
				case 2: //left
				{
					int NewY = RandY - 2;
					if (NewY <= 0)
						break;
					else
					{
						if (CellContainer.Contains(&Maze[RandX][RandY]) && CellContainer.Contains(&Maze[RandX][NewY]))
							break;
						else if (CheckClosedCells(RandX, RandY))
							break;
						else
						{
							CellContainer.Add(&Maze[RandX][RandY]);
							CellContainer.Add(&Maze[RandX][NewY]);
							count++;
							Maze[RandX][RandY] = false;
							Maze[RandX][NewY] = false;
							Maze[RandX][NewY + 1] = false;;
							break;
						}
					}
				}
				case 3: //up
				{
					int NewX = RandX - 2;
					if (NewX <= 0)
						break;
					else
					{
						if (CellContainer.Contains(&Maze[RandX][RandY]) && CellContainer.Contains(&Maze[NewX][RandY]))
							break;
						else if (CheckClosedCells(RandX, RandY))
							break;
						else
						{
							CellContainer.Add(&Maze[RandX][RandY]);
							CellContainer.Add(&Maze[NewX][RandY]);
							count++;
							Maze[RandX][RandY] = false;
							Maze[NewX][RandY] = false;
							Maze[NewX + 1][RandY] = false;
							break;
						}
					}
				}
				}

			}, EParallelForFlags::BackgroundPriority);
	}

	PrintMazeInLog();
	bIsGenerated = true;

}

void AUnrealThread2_2GameMode::SpawnWalls()
{
	FActorSpawnParameters SpawnParams;
	FTransform Transform;
	//Transform.SetRotation(FQuat);

	for (int i = 0; i < MazeSizeX; i++)
	{
		for (int j = 0; j < MazeSizeY; j++)
		{
			if (Maze[i][j])
			{
				if (!GetWorld() || !WallActor) return;

				Transform.SetLocation(FVector(i * 100.0f, j * 100.0f, 0.0f));
				GetWorld()->SpawnActor(WallActor, &Transform, SpawnParams);
			}
		}
	}
}

void AUnrealThread2_2GameMode::PrintMazeInLog()
{
	FString MazeString;
	MazeString += " \n";

	for (int i = 0; i < MazeSizeX; i++)
	{
		for (int j = 0; j < MazeSizeY; j++)
		{
			if (Maze[i][j])
			{
				MazeString += "X ";
			}
			else
			{
				MazeString += "  ";
			}
		}
		MazeString += "\n";
	}

	UE_LOG(LogTemp, Warning, TEXT("%s"), *MazeString);
}

bool AUnrealThread2_2GameMode::CheckClosedCells(int X, int Y)
{
	int localCount = 0;

	if (CellContainer.Contains(&Maze[X + 1][Y]))
	{
		localCount++;
	}

	if (CellContainer.Contains(&Maze[X - 1][Y]))
	{
		localCount++;
	}

	if (CellContainer.Contains(&Maze[X][Y + 1]))
	{
		localCount++;
	}

	if (CellContainer.Contains(&Maze[X][Y - 1]))
	{
		localCount++;
	}

	if (localCount >= 2)
		return true;

	return false;
}

void AUnrealThread2_2GameMode::Exits()
{
	//entering
	Maze[1][0] = false;
	Maze[1][1] = false;
	Maze[1][2] = false;
	Maze[1][3] = false;
	Maze[1][4] = false;
	Maze[1][5] = false;
	Maze[1][6] = false;
	Maze[1][7] = false;
	Maze[1][8] = false;
	Maze[2][1] = false;
	Maze[3][1] = false;
	Maze[4][1] = false;
	Maze[5][1] = false;
	Maze[6][1] = false;
	Maze[7][1] = false;
	Maze[8][1] = false;

	int x = MazeSizeX / 2;
	Maze[x][MazeSizeY - 1] = false;
	Maze[x][MazeSizeY - 2] = false;
	Maze[x][MazeSizeY - 3] = false;

	Maze[x - 1][MazeSizeY - 2] = false;
	Maze[x - 1][MazeSizeY - 3] = false;
	Maze[x - 1][MazeSizeY - 4] = false;
	Maze[x - 2][MazeSizeY - 2] = false;
	Maze[x - 2][MazeSizeY - 3] = false;
	Maze[x - 2][MazeSizeY - 4] = false;
	Maze[x - 3][MazeSizeY - 2] = false;
	Maze[x - 3][MazeSizeY - 3] = false;
	Maze[x - 3][MazeSizeY - 4] = false;
	Maze[x - 4][MazeSizeY - 2] = false;
	Maze[x - 4][MazeSizeY - 3] = false;
	Maze[x - 4][MazeSizeY - 4] = false;
	Maze[x + 1][MazeSizeY - 2] = false;
	Maze[x + 1][MazeSizeY - 3] = false;
	Maze[x + 1][MazeSizeY - 4] = false;
	Maze[x + 2][MazeSizeY - 2] = false;
	Maze[x + 2][MazeSizeY - 3] = false;
	Maze[x + 2][MazeSizeY - 4] = false;
	Maze[x + 3][MazeSizeY - 2] = false;
	Maze[x + 3][MazeSizeY - 3] = false;
	Maze[x + 3][MazeSizeY - 4] = false;
	Maze[x + 4][MazeSizeY - 2] = false;
	Maze[x + 4][MazeSizeY - 3] = false;
	Maze[x + 4][MazeSizeY - 4] = false;

	for (int i = 0; i < MazeSizeX; i++)
	{
		for (int j = 0; j < MazeSizeY; j++)
		{
			if (!Maze[i][j])
				CellContainer.Add(&Maze[i][j]);
		}
	}
}
