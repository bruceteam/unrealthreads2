// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealThread2_1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealThread2_1, "UnrealThread2_1" );
