// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealThread2_1GameMode.h"

void AUnrealThread2_1GameMode::DoThreads()
{
	ArrayOfIntegers.Empty();
	ArrayOfIntegers.SetNum(SizeOfArray);

	ParallelFor(SizeOfArray, [&](int32 TaskIndex)
		{
			ArrayOfIntegers[TaskIndex] = rand() % 100;
		}, EParallelForFlags::BackgroundPriority);

	PrintArray(false);

	ParallelFor(1, [&](int32 TaskIndex)
		{
			ArrayOfIntegers.Sort();
		}, EParallelForFlags::BackgroundPriority);

	PrintArray(true);
}


void AUnrealThread2_1GameMode::PrintArray(bool bIsSorted)
{
	FVector2D Scale = FVector2D(2, 2);
	FString StringFromArray;

	for (int i = 0; i < ArrayOfIntegers.Num(); i++)
	{
		StringFromArray += FString::Printf(TEXT("%d"), ArrayOfIntegers[i]);
		if (i < ArrayOfIntegers.Num() - 1)
			StringFromArray += "  ";
	}

	if (!bIsSorted)
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, FString::Printf(TEXT("Unordered Array is : % s"), *StringFromArray), false, Scale);
	else
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Green, FString::Printf(TEXT("Sorted Array is : % s"), *StringFromArray), false, Scale);

}
